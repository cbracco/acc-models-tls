!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  TT40-TI8 MADX model for Q26 optics post-LS2
!!
!! F.M. Velotti:
!! - Model from M. Fraser used for new TCDIL - adapted from Q20
!! - Used C. Hessler Q26 re-matching also considering LHC ATS optics
!! - PC added to re-match for new TCDIL locations (https://edms.cern.ch/ui/file/1458583/1.0/LHC-TCDI-ES-0004-1-0.pdf)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI8 line: LIU TCDIs for SPS Q26 optics";
/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt40ti8 ti8_repo";

set,    format="18.12f";
option, -echo, warn, info;

/***************************************
* Load model
***************************************/

call,   file = "ti8_repo/ti8.seq";
call,   file = "ti8_repo/ti8_apertures.dbx";
call,   file = "./ti8_q26.str";

option,  echo, warn, info;

beam,    sequence=ti8, particle=proton, pc= 450;
use,     sequence=ti8;

! Errors on b2 and b3 as measured
call, file="ti8_repo/mbi_b3_error.madx";


beta_Q26: beta0,   betx = 17.06248574,alfx =0.4588913839,
 bety=124.8219205, alfy=-3.444554843,
 dx=  -0.2527900995,dpx=  0.00334144588;


select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set,    format="27.17f";
Twiss,file="./twiss_ti8_q26_nom.tfs",beta0=beta_Q26; 

select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey,
          x0=x0.ti8, y0=y0.ti8, z0=z0.ti8,
          theta0=theta0.ti8, phi0=phi0.ti8, psi0=psi0.ti8;

write, table=survey, file="./survey_ti8.tfs";

/***********************************
* Cleaning up
***********************************/
system, "rm ti8_repo";
stop;