!==============================================================================================
! MADX file for LNE03 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNE03 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../lne lne_repo";

/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));

 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";
 
/*****************************************************************************
 * LNE00
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00_k.str";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;

 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lne.lne00.lne01, NEWNAME=lne00to01;

/*******************************************************************************
 * LNE01 line
 *******************************************************************************/
 call, file = "lne_repo/lne01/lne01.ele";
 call, file = "lne_repo/lne01/lne01_k.str";
 call, file = "lne_repo/lne01/lne01.seq";
 !call, file = "lne_repo/lne01/lne01.dbx"; !Presently no aperture database: to be updated
 
 EXTRACT, SEQUENCE=lne01, FROM=lne.start.0100, TO=lne.lne01.lne03, NEWNAME=lne01to03;

/*******************************************************************************
 * LNE03 line
 *******************************************************************************/
 call, file = "lne_repo/lne03/lne03.ele";
 call, file = "lne_repo/lne03/lne03_k.str";
 call, file = "lne_repo/lne03/lne03.seq";
 !call, file = "lne_repo/lne03/lne03.dbx"; !Presently no aperture database: to be updated


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lne00lne01lne03: sequence, refer=ENTRY, l = 10.6361309+11.91322192+8.034866818;
   lne00to01              , at =        0;
   lne01to03              , at = 10.6361309;
   lne03                  , at = 10.6361309+11.91322192;
  endsequence;

 SEQEDIT, SEQUENCE=lne00lne01lne03; FLATTEN; ENDEDIT;

/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./../stitched/elena_stitched.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * MATCH: uncomment to run matching
 *******************************************************************************

lne03match: macro={
select,flag=twiss,column=name,s,betx,bety,alfx,alfy,dx,dpx,dy,dpy,mux,muy,l,k0l,k1l;
OPTION, sympl = false;
twiss, beta0=initbeta0, file = "twiss_lne00_lne01_lne03_match.tfs";
};

use, sequence= lne00lne01lne03;
match,vlength=false,use_macro;

!vary,name=KLNE.ZQMF.0332,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMD.0333,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMF.0339,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMD.0340,step=1e-6,lower=-54,upper=54;

use_macro,name=lne03match;
emith = 6 ;
emitv = 4 ;
dppi = 2.5;
beamsize = 1;
constraint,weight=1,range=#e,expr= sqrt((table(twiss,LNE03$END,betx)*emith/6)+(beta*table(twiss,LNE03$END,dx)*dppi/4)^2) < beamsize;
constraint,weight=1,range=#e,expr= sqrt((table(twiss,LNE03$END,bety)*emitv/6)+(beta*table(twiss,LNE03$END,dy)*dppi/4)^2) < beamsize;

simplex,calls=10000,tolerance=1e-15;

endmatch;

*******************************************************************************/

/*******************************************************************************
 * twiss
 *******************************************************************************/
use, sequence= lne00lne01lne03;
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne00_lne01_lne03_nom.tfs";

/*************************************
* Survey
*************************************/
 set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_lne03.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";

stop;
