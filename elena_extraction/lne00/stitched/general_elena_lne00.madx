!==============================================================================================
! MADX file for LNE00 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "ELENA/LNE00 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../elena elena_repo";
system, "ln -fns ./../../lne lne_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For now only macro to evaluate changes in ZDFA.0310
*****************************************************************************/

call, file = "lne_repo/load_elena_extraction00.madx";

! It needs as input as ZDFA.0310 delta kick (absolute value in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! if "sign" (second argument) = 1, positive kick, negative otherwise

set, format="22.6e";
exec, calculate_extraction(0e-3, 1, twiss_elena_stitched.tfs);

exec, set_ini_conditions();

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/

assign, echo="elena_stitched.inp";

betx0 = tl_initial_cond->betx;
bety0 =  tl_initial_cond->bety;

alfx0 = tl_initial_cond->alfx;
alfy0 = tl_initial_cond->alfy;

dx0 = tl_initial_cond->dx;
dy0 = tl_initial_cond->dy;

dpx0 = tl_initial_cond->dpx;
dpy0 = tl_initial_cond->dpy;

x0 = tl_initial_cond->x;
px0 = tl_initial_cond->px;


print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of ELENA extraction to LNE00";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNE00
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00_k.str";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;


/*******************************************************************************
 * Run twiss for LNE00 and stitch result
 *******************************************************************************/

use, sequence= lne00;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne00.tfs";

! Make one single tfs file for both ring and FTN transfer line using kickers
len_twiss_tl = table(twiss, tablelength);

i = 2;
option, -info;
while(i < len_twiss_tl){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_elena_lne00_nom_complete.tfs";

/*************************************
* Cleaning up
*************************************/

system, "rm lne_repo";
system, "rm elena_repo";

stop;
